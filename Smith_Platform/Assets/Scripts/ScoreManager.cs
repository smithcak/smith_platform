﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {


	public static int scoreCount;

	public Text text;



	void Awake()
	{
		text = GetComponent<Text>();
		scoreCount = 0;
	}


	void Start()
	{

	}

	void Update()
	{
		if (scoreCount < 0) 
			scoreCount = 0;


		text.text = "" + scoreCount;
	}


	public static void AddPoints (int pointsToAdd)
	{
		scoreCount += pointsToAdd;
	}

	public static void Reset()
	{
		scoreCount = 0;

	}
		
}