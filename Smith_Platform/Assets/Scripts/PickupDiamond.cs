﻿using UnityEngine;
using System.Collections;

public class PickupDiamond : MonoBehaviour {

	public int pointsToAdd;
	public static int scoreCount;

	// Use this for initialization
	void Start () {

		ScoreManager.scoreCount = 0;

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player"))
			Destroy (gameObject);
			HiScoreManager.scoreCount += 40;

	}
}
